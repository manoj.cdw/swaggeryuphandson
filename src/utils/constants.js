export const NOT_FOUND = 404;
export const BAD_REQUEST = 400;
export const NO_CONTENT = 204;
