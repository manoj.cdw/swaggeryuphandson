import { readFile, writeFile } from "node:fs/promises";
import { logger } from "../logger/logger.js";

const databaseFile = "./database/cdw_ace23_buddies.json";

// Load data from json file
export async function loadDataBase() {
  let data;

  try {
    data = (await readFile(databaseFile)) || "[]";
  } catch (err) {
    await writeFile(databaseFile, "[]");
    logger.error(err.message);
  }
  return JSON.parse((await readFile(databaseFile)) || "[]");
}

// Save data to json file
export async function saveDataBase(data) {
  await writeFile(databaseFile, JSON.stringify(data, null, 4));
}
