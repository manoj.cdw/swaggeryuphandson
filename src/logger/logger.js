import winston from "winston";
import { config } from "dotenv";
config();

// Logger
export const logger = winston.createLogger({
  level: process.env.LOG_LEVEL ?? "info",
  format: winston.format.combine(
    winston.format.timestamp({
      format: "YYYY-MM-DD hh:mm:ss.SSS A",
    }),
    // winston.format.align(),
    winston.format.json(),
  ),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({
      level: "info",
      filename: "./logs/info.log",
    }),
    new winston.transports.File({ level: "warn", filename: "./logs/warn.log" }),
    new winston.transports.File({
      level: "error",
      filename: "./logs/error.log",
    }),
  ],
});
