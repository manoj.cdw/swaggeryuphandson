import { Router } from "express";
import {
  addBuddy,
  deleteBuddyController,
  listBuddies,
  listSingleBuddy,
  updateBuddyController,
} from "../controllers/buddyController.js";
import { validateBuddyData, validateBuddyUpdateData, validateId } from "../middleware/validation.js";

const app = Router();
/**
 * @swagger
 * /buddy/list:
 *   get:
 *     summary: Get all buddies
 *     description: Retrieve a list of all buddies.
 *     responses:
 *       '200':
 *         description: Successful response with list of buddies
 *       '404':
 *         description: No buddies found
 */
app.get("/list", listBuddies);

/**
 * @swagger
 * /buddy/add:
 *   post:
 *     summary: Add a new buddy
 *     description: Add a new buddy to the system.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: Real name of the buddy
 *               nickname:
 *                 type: string
 *                 description: Real name of the buddy
 *               dob:
 *                 type: string
 *                 description: Real name of the buddy
 *               hobbies:
 *                 type: string
 *                 description: Real name of the buddy
 *     responses:
 *       '200':
 *         description: Successful response with updated buddies list
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 name:
 *                   type: string
 *                   description: name of the buddy
 *                 nickname:
 *                   type: string
 *                   description: nickname of the buddy
 *                 dob:
 *                   type: string
 *                   description: date of birth of the buddy
 *                 hobbies:
 *                   type: string
 *                   description: hobbies of the buddy
 *       '400':
 *         description: Invalid request body
 */
app.post("/add", validateBuddyData, addBuddy);

/**
 * @swagger
 * /buddy/update/{id}:
 *   put:
 *     summary: Update a buddy
 *     description: Update information of an existing buddy.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Employee ID of the buddy to update
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               nickname:
 *                 type: string
 *                 description: Real name of the buddy
 *               hobbies:
 *                 type: string
 *                 description: Real name of the buddy
 *     responses:
 *       '200':
 *         description: Successful response indicating buddy information updated
 *       '404':
 *         description: Buddy not found
 */
app.put("/update/:id", validateId,validateBuddyUpdateData,updateBuddyController);

/**
 * @swagger
 * /buddy/delete/{id}:
 *   delete:
 *     summary: Delete a buddy
 *     description: Remove a buddy from the system.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Employee ID of the buddy to delete
 *     responses:
 *       '200':
 *         description: Successful response indicating buddy deleted
 *       '404':
 *         description: Buddy not found
 */
app.delete("/delete/:id", validateId, deleteBuddyController);

app.get("/:id", validateId, listSingleBuddy);

export default app;
