import { logger } from "../logger/logger.js";
import { NOT_FOUND } from "../utils/constants.js";
import { loadDataBase, saveDataBase } from "../utils/databaseUtils.js";
import { v4 as uuidv4 } from "uuid";

// Returns the list of all buddies available in the database
export async function getAllBuddies() {
  logger.info("getAllBuddies Service started");
  logger.info("getAllBuddies Service ended");
  return await loadDataBase();
}

// Returns the buddy with the given ID
// Returns Not found if buddy with the id is not found
export async function getBuddyById(buddyId) {
  logger.info("getBuddyById Service started");
  const buddies = await loadDataBase();
  const buddy = buddies.find((buddy) => buddy.id === buddyId);
  if (buddy === undefined) return NOT_FOUND;
  logger.info("getBuddyById Service ended");
  return buddy;
}

// Returns the buddy with the given name
// Returns Not found if buddy with the name is not found
export async function getBuddyByName(buddyName) {
  logger.info("getBuddyByName Service started");
  const buddies = await loadDataBase();
  const buddy = buddies.find((buddy) => buddy.name === buddyName);
  if (buddy === undefined) return NOT_FOUND;
  logger.info("getBuddyByName Service ended");
  return buddy;
}

// Save the buddy details to the database
export async function saveBuddy(buddy) {
  logger.info("saveBuddy Service started");
  const buddies = await loadDataBase();
  buddies.push({ ...buddy, id: uuidv4() });
  await saveDataBase(buddies);
  logger.info("saveBuddy Service ended");
}

// Update the nickname and hobbies of buddy
export async function updateBuddy(buddyId, buddyDetails) {
  logger.info("updateBuddy Service started");
  const buddies = await loadDataBase();
  const oldBuddyIndex = buddies.findIndex((buddy) => buddy.id === buddyId);
  if (oldBuddyIndex === undefined) return NOT_FOUND;
  const oldBuddy = buddies[oldBuddyIndex];
  buddies.splice(oldBuddyIndex, 1, {
    ...oldBuddy,
    nickName: buddyDetails.nickName,
    hobbies: buddyDetails.hobbies,
    id: buddyId,
  });
  await saveDataBase(buddies);
  logger.info("updateBuddy Service ended");
}

// Delete the buddy from the database
export async function deleteBuddy(buddyId) {
  logger.info("deleteBuddy Service started");
  const buddies = await loadDataBase();

  const buddyIndex = buddies.findIndex((buddy) => buddy.id === buddyId);
  if (buddyIndex === -1) return NOT_FOUND;
  console.log(buddyId, buddyIndex);

  buddies.splice(buddyIndex, 1);
  await saveDataBase(buddies);
  logger.info("deleteBuddy Service ended");
}
