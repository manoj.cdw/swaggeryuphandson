import * as yup from "yup";

export const buddyDetailsSchema = yup.object({
  name: yup.string().required(),
  nickname: yup.string().required(),
  dob: yup.date().required(),
  hobbies: yup.string().required(),
});


export const updateDetailsSchema = yup.object({
  nickname: yup.string().required(),
  hobbies: yup.string().required(),
});
