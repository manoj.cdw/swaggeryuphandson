import { validate } from "uuid";
import moment from "moment";
import { logger } from "../logger/logger.js";
import { buddyDetailsSchema } from "../schemas/buddyDetails.js";

// Validate Id field from the request body
export function validateId(req, res, next) {
  logger.info("Validate Id middleware started");
  console.log("hello World");
  console.log(req.params);
  if (!validate(req.params.id)) {
    res.status(404).send("Invalid id");
    return;
  }
  req.buddyId = req.params.id;
  next();
  logger.info("Validate Id middleware ended");
}

// Validate the buddy data fields from the request body
export async function validateBuddyData(req, res, next) {
  logger.info("Validate Buddy Data middleware started");

  try {
    const buddy = await buddyDetailsSchema.validate(req.body);
    console.log(buddy);
  } catch (error) {
    res.status(400).json({ error: error.message });
    return;
  }

  if (!moment(req.body.dob, "D/M/YYYY", true).isValid()) {
    res.status(400).send("Invalid DOB");
    return;
  }
  if (!moment(req.body.dob, "D/M/YYYY", true).isBefore(new Date())) {
    res.status(400).send("DOB must be in the past");
    return;
  }

  req.buddy = {
    name: req.body.name,
    nickName: req.body.nickname,
    DOB: req.body.dob,
    hobbies: req.body.hobbies,
  };
  next();
  logger.info("Validate Buddy Data middleware Ended");
}


export async function validateBuddyUpdateData(req, res, next) {
  logger.info("Validate Buddy Update Data middleware started");

  try {
    await buddyDetailsSchema.validate(req.body);
  } catch (error) {
    res.status(400).json({ error: error.message });
    return;
  }

  if (req.body.dob && (!moment(req.body.dob, "D/M/YYYY", true).isValid() || !moment(req.body.dob, "D/M/YYYY", true).isBefore(new Date()))) {
    res.status(400).send("Invalid or future DOB");
    return;
  }

  req.buddy = {
    nickname: req.body.nickname,
    hobbies: req.body.hobbies,
  };

  next();
  logger.info("Validate Buddy Update Data middleware ended");
}