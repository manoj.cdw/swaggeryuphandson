import { logger } from "../logger/logger.js";
import {
  deleteBuddy,
  getAllBuddies,
  getBuddyById,
  getBuddyByName,
  saveBuddy,
  updateBuddy,
} from "../services/buddyServices.js";
import { NOT_FOUND } from "../utils/constants.js";
import { formatResponse } from "../utils/loggerUtils.js";

export async function listBuddies(req, res) {
  logger.info("List Buddies Controller Started");
  const buddies = await getAllBuddies();
  if (buddies.length === 0) {
    logger.warn(
      formatResponse(NO_CONTENT, constants.noBuddiesError, req.method),
    );
    res.status(NO_CONTENT).send(constants.noBuddiesError);
  } else res.send(buddies);
  logger.info("List Buddies Controller Ended");
}

export async function listSingleBuddy(req, res) {
  logger.info("List Single Buddies Controller Started ");
  let buddy;
  if (!!req.buddyId) {
    buddy = await getBuddyById(req.buddyId);
    if (buddy === NOT_FOUND) {
      logger.warn(
        formatResponse(NOT_FOUND, constants.noBuddyWithIdError, req.method),
      );
      return res.status(NOT_FOUND).send(constants.noBuddyWithIdError);
    }
  } else {
    buddy = await getBuddyByName(req.buddyName);
    if (buddy === NOT_FOUND) {
      logger.warn(
        formatResponse(NOT_FOUND, constants.noBuddyWithNameError, req.method),
      );
      return res.status(NOT_FOUND).send(constants.noBuddyWithNameError);
    }
  }
  res.send(buddy);
  logger.info("List Single Buddies Controller Ended");
}

export async function addBuddy(req, res) {
  logger.info("Add Buddy Controller Started");
  if ((await getBuddyByName(req.buddy.name)) !== NOT_FOUND) {
    logger.warn(
      formatResponse(
        NOT_FOUND,
        constants.buddyWithSameNameExistsError,
        req.method,
      ),
    );
    res.status(NOT_FOUND).send(constants.buddyWithSameNameExistsError);
    return;
  }
  saveBuddy(req.buddy);
  res.send("Created buddy successflly");
  logger.info("Add Buddy Controller Ended");
}

export async function updateBuddyController(req, res) {
  logger.info("Update Buddy Controller Started");
  if (getBuddyById(req.buddyId) === NOT_FOUND) {
    logger.warn(
      formatResponse(NOT_FOUND, constants.noBuddyWithIdError, req.method),
    );
    res.status(NOT_FOUND).send(constants.noBuddyWithIdError);
    return;
  }
  updateBuddy(req.buddyId, req.buddy);
  res.send("Update Buddy Details successfully");
  logger.info("Update Buddy Controller Ended");
}

export async function deleteBuddyController(req, res) {
  logger.info("Delete Buddy Controller Started");
  if ((await getBuddyById(req.buddyId)) === NOT_FOUND) {
    logger.warn(
      formatResponse(NOT_FOUND, constants.noBuddyWithIdError, req.method),
    );
    res.status(NOT_FOUND).send(constants.noBuddyWithIdError);
    return;
  }

  deleteBuddy(req.buddyId);
  res.send("deleted buddy successfully");
  logger.info("Delete Buddy Controller Ended");
}
