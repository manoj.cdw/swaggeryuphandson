import { logger } from "../logger/logger.js";

export function errorController(req, res) {
  logger.info("ErrorController started");
  res.status(404).send("Invalid Route");
  logger.info("ErrorController Ended");
}
