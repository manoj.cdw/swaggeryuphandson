// import * as swaggerJsdoc from "swagger-jsdoc";
import swaggerJsDoc from "swagger-jsdoc";

const swaggerOptions = {
  swaggerDefinition: {
    openapi: "3.0.0",
    info: {
      title: "Buddies API",
      description: "API for managing buddies",
      contact: {
        name: "team-A",
      },
      servers: [`http://localhost:3000`],
    },
  },
  apis: ["./src/routes/buddyRouter.js"],
};

export const swaggerDocs = swaggerJsDoc(swaggerOptions);
