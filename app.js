import express from "express";
import cors from "cors";
import { config } from "dotenv";
import buddyRouter from "./src/routes/buddyRouter.js";
import { writeFileSync } from "node:fs";
import { errorController } from "./src/controllers/errorController.js";
import { logger } from "./src/logger/logger.js";
import { swaggerDocs } from "./src/swagger/swagger.js";
import * as swaggerUI from "swagger-ui-express";

config();
// writeFileSync("./database/cdw_ace23_buddies.json", "[]");

// swaggerSetup(app);

const app = express();
app.use(express.json());
app.use((req, res, next) => {
  logger.info(
    "Received request from " +
      req.ip +
      " for route " +
      req.originalUrl +
      " with method " +
      req.method,
  );
  next();
});
app.use(cors());

app.use("/buddy", buddyRouter);
app.use("/buddies-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocs));
app.use("*", errorController);

app.listen(process.env.PORT ?? 3000, () => {
  logger.info("Server Started");
});
